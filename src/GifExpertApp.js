import React, { useState } from 'react';
import { AddCategory } from './components/AddCategory';
import { GifGrid } from './components/GifGrid';


//const categories=["one punch","samurai x","dragon ball"];


export const GifExpertApp = () => {

    const [categories, setCategories] = useState(["one punch"]);

    //const handleAdd=()=>{
    //   setCategories(["hunterx",...categories]);
    //  }

    return (
        <div>
            <h2>GifExpertApp by John Mejia</h2>;
            <AddCategory setCategories={setCategories} />
            <hr />



            <ol>
                {
                categories.map(category =>(
                    <GifGrid 
                    key={category}
                    category={category}/>
                ))
              }
            </ol>
                
        </div>
    )
}
